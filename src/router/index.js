import {createRouter, createWebHistory} from "vue-router";
import HomeView from "@/views/HomeView.vue";
import PersonListView from "@/views/PersonListView.vue";
import PersonDetailView from "@/views/PersonDetailView.vue";

const routes = [
    {
        path: '/',
        component: HomeView
    },
    {
        path: '/persons',
        component: PersonListView
    },
    {
        path: '/persons/:personId',
        component: PersonDetailView
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export {router} // <- wird der main.js zur Verfügung gestellt
