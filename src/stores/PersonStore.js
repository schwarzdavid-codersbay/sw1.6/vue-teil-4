import {defineStore} from "pinia";
import axios from "axios";

const usePersonsStore = defineStore('persons', {
    state: () => ({
        persons: []
    }),
    actions: {
        loadAllPersons: async function () {
            const userResponse = await axios.get('https://pavildgaiz.user-management.asw.rest/api/users')
            this.persons = userResponse.data
        },
        addPerson: async function (userData) {
            const userResponse = await axios.post('https://pavildgaiz.user-management.asw.rest/api/users', userData)
            this.persons.push(userResponse.data)
        }
    }
})

export {usePersonsStore}


/*function state() {
    return 1;
}
const state = () => {
    return {
        persons: []
    }
};

const state = () => ({
    persons: []
})*/
